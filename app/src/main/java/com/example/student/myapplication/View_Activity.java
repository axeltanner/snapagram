package com.example.student.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class View_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewFragment viewFragment = new ViewFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, viewFragment).commit();
    }
}
