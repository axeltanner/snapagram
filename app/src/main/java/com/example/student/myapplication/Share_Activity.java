package com.example.student.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Share_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ShareFragment shareFragment = new ShareFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container,shareFragment).commit();
    }
}
