package com.example.student.myapplication;

public class Constants {
    public static final String ACTION_ADD_FRIEND = "com.example.student.myapplication.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "com.example.student.myapplication.SEND_FRIEND_REQUEST";

    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "com.example.student.myapplication.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "com.example.student.myapplication.ADD_FRIEND_FAILURE";

    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "com.example.student.myapplication.FRIEND_REQUEST_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "com.example.student.myapplication.FRIEND_REQUEST_FAILURE";
}
